from django.urls import path
from rest_framework import routers

from .views import stock

router = routers.SimpleRouter()
router.register(r'stocks', stock.StockViewset, basename='stock')
urlpatterns = router.urls
