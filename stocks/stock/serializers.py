
from rest_framework import serializers
from .models import Stock, Portfolio


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = ['id', 'name', 'price', 'created_on', 'updated_on']

class PortfolioSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)
    stock = serializers.StringRelatedField(read_only=True)
    
    class Meta:
        model = Portfolio
        fields = ['id', 'total', 'price', 'created_on', 'updated_on']
