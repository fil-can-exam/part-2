from rest_framework import viewsets, status
from rest_framework.response import Response
from ..models import Stock
from ..serializers import StockSerializer

class StockViewset(viewsets.ViewSet):
    def list(self, request):
        stocks = Stock.objects.all()
        serializer = StockSerializer(stocks, many=True)

        return Response(serializer.data)

    def create(self, request):
        inputData = request.data

        director = Stock.objects.create(
            name=inputData['name'],
            price=inputData['price'],
        )
        serializer = StockSerializer(director)

        return Response(serializer.data)