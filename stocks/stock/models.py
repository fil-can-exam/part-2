from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Stock(models.Model):
    name=models.CharField(max_length=200, null=False)
    price=models.FloatField(null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Portfolio(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE, null=False)
    total=models.FloatField(null=False)
    unit=models.BigIntegerField(null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
