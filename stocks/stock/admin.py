import site
from django.contrib import admin
from stock.models import Stock, Portfolio

# Register your models here.
admin.site.register(Stock)
admin.site.register(Portfolio)
